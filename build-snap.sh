#!/bin/sh
#this script is intended to be called via snapcraft docker container,
# e.g. docker run -it --rm -v $CI_PROJECT_DIR:/mnt vagtsi/snapcraft-arm64 /mnt/build-snap.sh

ARCH=`uname -m`
echo "Executing script $0 on a $ARCH machine on system:"
lsb_release -cir
echo "Configured system package sources:"
apt-cache policy
SCRIPT_DIR=`dirname $0`
cd $SCRIPT_DIR
echo "Building snap in directory $PWD on a $ARCH machine"
snapcraft --version
sed -e s/\$VERSION/1.0.0/ -e s/\$TARGET_ARCH/amd64/ -e s/\$SOURCE_ARCH/amd64/ -e s/\$NODE_ARCH/x64/ snapcraft-template.yaml > snap/snapcraft.yaml
snapcraft
