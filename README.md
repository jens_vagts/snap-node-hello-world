# Snap Node Hello World

Minimal test scenario for building a nodejs typescript app snap for a arm64 device incl. some system dependencies requiring
additional/enhanced system access rights.

## Project structure
Two nodejs projects are defined within subfolders:
- `hello-world-js`: JavaScript project just printing the CPU architecture to the console/stdout and testing required system dependencies.
   This is for first testing only, as the aim of this project is to test TypeScript!
- `hello-world-ts`: The TypeScript project to be packaged as snap. Prints some system informations
   and tests all required system dependencies/libraries.
   This needs an additional compilation step via `tsc index.ts` after the initial `npm install` call.
   For convienience the script `build` has been prepared in the projects `package.json` which can be invoked via `npm run build` from the command line.

## Building snap locally with docker
The `snapcraft.yaml` is provided as a template named `snapcraft-template.yaml` in the project base directory.
It contains some variables in order to support different build scenarios and target architecture. Please 
use one of the following commands from the project base directory to created the snacraft definition for your specific use case:
- on **AMD**64: `sed -e s/\$VERSION/1.0.0/ -e s/\$TARGET_ARCH/amd64/ -e s/\$SOURCE_ARCH/amd64/ -e s/\$NODE_ARCH/x64/ snapcraft-template.yaml > snap/snapcraft.yaml`
- on **ARM**64: `sed -e s/\$VERSION/1.0.0/ -e s/\$TARGET_ARCH/arm64/ -e s/\$SOURCE_ARCH/amd64/ -e s/\$NODE_ARCH/arm64/ snapcraft-template.yaml > snap/snapcraft.yaml`

Use following command to build the ARM64 snap with your local docker environment:
- on **AMD**64: `sudo docker run -it --rm -w /mnt -v $PWD:/mnt vagtsi/snapcraft-core18 snapcraft --target-arch=arm64`
- on **ARM**64: `sudo docker run -it --rm -w /mnt -v $PWD:/mnt vagtsi/snapcraft-arm64 snapcraft`

**Note**: the original snapcraft docker image `snapcore/snapcraft` can not be used as it does **not** support the required base `core18`
and it does not support the necessary multi architecture/cross building of the targeted `arm64` snap!
The project [Snapcraft core18](https://gitlab.com/jens_vagts/snapcraft-core18) does provide an _Ubuntu Bionic_ based docker image
supporting `arm64` as target architecture. The already built docker image is avaible on the docker hub as
[vagtsi/snapcraft-core18](https://hub.docker.com/repository/docker/vagtsi/snapcraft-core18).

## Additional sources used in this project
- [Snapcraft arm64 docker image](https://gitlab.com/jens_vagts/snapcraft-arm64)
- [Docker Engine on Intel Linux runs Arm Containers](https://blog.hypriot.com/post/docker-intel-runs-arm-containers/)
